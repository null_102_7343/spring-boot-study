package org.LionZhou.study;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;

public class MyList {

    void skipList() {
        ConcurrentSkipListMap<String, String> skipListMap = new ConcurrentSkipListMap<>();
        skipListMap.put("a", "hello");

        System.out.println(skipListMap);
    }

    void map() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("a", "map");
        System.out.println(hashMap);
    }

    void mutableKeyHashMap() {
        HashMap<Set, String> hashMap = new HashMap<>();
        HashSet<String> hashSet = new HashSet<>();
        HashSet<String> hashSet2 = new HashSet<>();

        hashSet.add("1");
        hashSet2.add("1");

        hashMap.put(hashSet, "mutable");
        System.out.println(hashMap);

        System.out.println(hashSet2);
    }

    final void noOverride(){

    }

    public class MyListChild extends MyList {
        final void noOverride(int a, int b) {

        }
    }

    public static void main(String[] args) {
        MyList myList = new MyList();
        myList.mutableKeyHashMap();
    }
}
