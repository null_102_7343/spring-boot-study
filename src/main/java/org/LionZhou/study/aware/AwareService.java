package org.LionZhou.study.aware;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * BeanNameAware 获取 spring 的bean 名字服务
 * ResourceLoaderAware 获取 spring 的资源家在服务
 */
@Service
public class AwareService implements BeanNameAware, ResourceLoaderAware {

    private String beanName;
    private ResourceLoader resourceLoader;

    @Override
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public void outputResult() {
        System.out.println("Bean name is '" + beanName + "'");

        Resource resource = resourceLoader.getResource("classpath:org/cutelion/study/aware/resource.txt");
        try {
            String s = IOUtils.toString(resource.getInputStream());
            System.out.println("resource file content is '" + s + "'");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
