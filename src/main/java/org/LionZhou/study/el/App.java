package org.LionZhou.study.el;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ELConfig.class);

        ELConfig elConfig = context.getBean(ELConfig.class);
        elConfig.outputResource();

        context.close();
    }
}
