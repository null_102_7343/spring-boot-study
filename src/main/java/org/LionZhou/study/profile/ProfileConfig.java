package org.LionZhou.study.profile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class ProfileConfig {

    @Bean
    @Profile("dev")
    public Demo devDemo() {
        Demo demo = new Demo();
        demo.setMsg("from dev");
        return demo;
    }

    @Bean
    @Profile("prod")
    public Demo prodDemo() {
        Demo demo = new Demo();
        demo.setMsg("from prod");
        return demo;
    }
}
