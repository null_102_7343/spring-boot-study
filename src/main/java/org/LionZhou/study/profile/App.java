package org.LionZhou.study.profile;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {
        // 如果将配置文件直接作为扩展配置文件作为构造参数传入,后面再设置 profile 就没用了
//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ProfileConfig.class);
//        context.getEnvironment().setActiveProfiles("prod");

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles("dev");
        context.register(ProfileConfig.class);
        context.refresh();

        Demo bean = context.getBean(Demo.class);
        System.out.println(bean.getMsg());

        context.close();
    }
}
