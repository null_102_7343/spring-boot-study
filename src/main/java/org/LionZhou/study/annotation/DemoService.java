package org.LionZhou.study.annotation;

import org.springframework.stereotype.Service;

@Service
public class DemoService {

    public void outputResult() {
        System.out.println("from compose annotation.");
    }
}
