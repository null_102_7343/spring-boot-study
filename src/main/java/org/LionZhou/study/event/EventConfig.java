package org.LionZhou.study.event;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.cutelion.study.event")
public class EventConfig {
}
