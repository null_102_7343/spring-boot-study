package org.LionZhou.study.event;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(EventConfig.class);

        DemoPublish publish = context.getBean(DemoPublish.class);
        publish.publish("hello");

        context.close();
    }
}
