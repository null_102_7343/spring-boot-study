package org.LionZhou.study.tomcat;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Configuration;

/**
 * spring 1.x EmbeddedServletContainerCustomizer 实现该类.
 *
 * 在 spring 2 中,通过实现 {@link WebServerFactoryCustomizer<ConfigurableServletWebServerFactory>}
 * 接口,来实现基于 java 代码的web 设置.
 */
@Configuration
public class ConfigEmbeddedServletContainerCustomize implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {

    @Override
    public void customize(ConfigurableServletWebServerFactory factory) {
        ((TomcatServletWebServerFactory) factory).addConnectorCustomizers(new TomcatConnectorCustomizer() {
            @Override
            public void customize(Connector connector) {
                Http11NioProtocol protocolHandler = (Http11NioProtocol) connector.getProtocolHandler();
                protocolHandler.setMaxConnections(200);
                protocolHandler.setMaxThreads(200);
                protocolHandler.setSelectorTimeout(3000);
                protocolHandler.setSessionTimeout(3000);
                protocolHandler.setConnectionTimeout(3000);

                connector.setPort(8888);
            }
        });
    }
}
