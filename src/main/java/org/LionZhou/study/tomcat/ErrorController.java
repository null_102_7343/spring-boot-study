package org.LionZhou.study.tomcat;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller("/")
public class ErrorController {

    /**
     * 404 error
     *
     * @return
     */
    @RequestMapping("404")
    public String error404() {
        return "404.html";
    }

    /**
     * 500 error
     *
     * @return
     */
    @RequestMapping("500")
    public String error500() {
        return "500.html";
    }

    @GetMapping("fuck")
    @ResponseBody
    public String fuck() {
        return "fuck";
    }
}
