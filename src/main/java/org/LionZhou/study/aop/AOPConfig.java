package org.LionZhou.study.aop;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configurable
@ComponentScan("org.cutelion.blog.study.aop")
@EnableAspectJAutoProxy
public class AOPConfig {

}
