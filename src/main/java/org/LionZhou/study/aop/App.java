package org.LionZhou.study.aop;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AOPConfig.class);
        DemoAnnotationService service = context.getBean(DemoAnnotationService.class);
        DemoAnnotationMethod method = context.getBean(DemoAnnotationMethod.class);

        service.addService();
        method.add();

        context.close();
    }
}
