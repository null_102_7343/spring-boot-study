package org.LionZhou.study.banner;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;

public class App {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(App.class);
        springApplication.setBannerMode(Banner.Mode.OFF);
        springApplication.run(args);

        // form with fluent api
//        new SpringApplicationBuilder(App.class)
//                .bannerMode(Banner.Mode.OFF)
//                .run(args);
    }
}
