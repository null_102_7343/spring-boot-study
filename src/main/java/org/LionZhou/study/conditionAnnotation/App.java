package org.LionZhou.study.conditionAnnotation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConditionConfig.class);

        ListService listService = context.getBean(ListService.class);

        String property = context.getEnvironment().getProperty("os.name");
        System.out.println(property);
        if (property != null) {
            System.out.println("os " + property + ":" + listService.showListCmd());
        }

        context.close();
    }
}
