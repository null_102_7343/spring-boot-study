package org.LionZhou.study.conditionAnnotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConditionConfig {

    @Bean
    @Conditional(WindowsCondition.class)
    public ListService windowsListService() {
        return new WindowsList();
    }

    /**
     * {@link Conditional} 中指定 bean 的装配条件.
     *
     * @return
     */
    @Bean
    @Conditional(LinuxCondition.class)
    ListService linuxListService() {
        return new LinuxList();
    }

    @Bean
    @Conditional(MacCondition.class)
    ListService macListService() {
        return new MacList();
    }
}
