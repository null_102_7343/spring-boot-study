package org.LionZhou.study.conditional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.filter.OrderedCharacterEncodingFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.annotation.Resource;

@Configuration
@EnableConfigurationProperties(HttpEncodingAutoConfiguration.class)
// 当 CharacterEncodingFilter.class 在类路径的条件下
@ConditionalOnClass(CharacterEncodingFilter.class)
// if spring.http.encoding == "enabled" 时
@ConditionalOnProperty(prefix = "spring.http.encoding", value = "enabled", matchIfMissing = true)
public class HttpEncodingAutoConfiguration {

    @Resource
    private HttpEncodingProperties httpEncodingProperties;

    /**
     * 当容器中不存在{@link CharacterEncodingFilter}.class 这个bean 的时候新建 bean
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(CharacterEncodingFilter.class)
    public CharacterEncodingFilter characterEncodingFilter() {
        OrderedCharacterEncodingFilter orderedCharacterEncodingFilter = new OrderedCharacterEncodingFilter();
        orderedCharacterEncodingFilter.setEncoding(this.httpEncodingProperties.getCharset().name());
        orderedCharacterEncodingFilter.setForceEncoding(this.httpEncodingProperties.isForce());
        return orderedCharacterEncodingFilter;
    }
}
