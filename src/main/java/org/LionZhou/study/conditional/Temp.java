package org.LionZhou.study.conditional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnJava;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.system.JavaVersion;

public class Temp {

    @ConditionalOnJava(JavaVersion.EIGHT)
    public void onJavaVersionEight() {

    }

    @ConditionalOnWebApplication
    public void onWebApplication() {

    }
}
