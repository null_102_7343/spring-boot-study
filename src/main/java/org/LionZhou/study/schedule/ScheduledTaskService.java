package org.LionZhou.study.schedule;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 该定时任务是单线程的任务队列,因此如果有长耗时任务,会导致任务被堵塞,其他任务都不执行,但是任务会被添加到队列中
 * 导致后续多个任务同时执行
 */
@Service
public class ScheduledTaskService {

    private static final SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");

    /**
     * 间隔时间执行
     */
    @Scheduled(fixedRate = 1000)
    public void reportCurrentTime() {
        System.out.println("interval 1s:" + df.format(new Date()));
    }

    @Scheduled(fixedRate = 3000)
    public void blockScheduled() {
        try {
            System.out.println("block 5s.");
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 类 unix 的 crontable
     * 秒 分 时 日 月 星期(eg.MON-FRI) / 年
     */
    @Scheduled(cron = "*/5 * * * * *")
    public void fixTimeExecution() {
        System.out.println("in special time: " + df.format(new Date()) + " execution.");
    }
}
