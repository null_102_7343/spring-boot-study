package org.LionZhou.study.schedule;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(TaskSchedulerConfig.class);

        ScheduledTaskService scheduledTaskService = context.getBean(ScheduledTaskService.class);

    }
}
