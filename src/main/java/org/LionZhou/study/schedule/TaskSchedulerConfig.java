package org.LionZhou.study.schedule;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@ComponentScan("org.cutelion.study.schedule")
@EnableScheduling
public class TaskSchedulerConfig {
}
