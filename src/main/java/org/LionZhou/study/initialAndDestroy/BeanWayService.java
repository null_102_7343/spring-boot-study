package org.LionZhou.study.initialAndDestroy;

public class BeanWayService {

    public void init() {
        System.out.println("init");
    }

    public void destroy() {
        System.out.println("destroy");
    }
}
