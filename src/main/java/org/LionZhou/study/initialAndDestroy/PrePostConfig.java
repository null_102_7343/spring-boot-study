package org.LionZhou.study.initialAndDestroy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.cutelion.study.initialAndDestroy")
public class PrePostConfig {

    @Bean(initMethod = "init", destroyMethod = "destroy")
    BeanWayService getBeanWayService() {
        return new BeanWayService();
    }

    @Bean
    JSR250WayService getJSR250WayService() {
        return new JSR250WayService();
    }
}
