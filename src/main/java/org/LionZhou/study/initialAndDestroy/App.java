package org.LionZhou.study.initialAndDestroy;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(PrePostConfig.class);

        JSR250WayService jsr250WayService = context.getBean(JSR250WayService.class);
        BeanWayService beanWayService = context.getBean(BeanWayService.class);

        context.close();
    }
}
