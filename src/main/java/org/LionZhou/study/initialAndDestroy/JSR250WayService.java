package org.LionZhou.study.initialAndDestroy;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class JSR250WayService {

    @PostConstruct
    public void init() {
        System.out.println("post init");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("pre destroy");
    }
}
