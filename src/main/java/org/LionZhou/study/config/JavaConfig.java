package org.LionZhou.study.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JavaConfig {

    @Bean
    public HelloJava helloJava() {
        return new HelloJava();
    }

    @Bean
    public DoubleHelloJava doubleHelloJava(HelloJava helloJava) {
        DoubleHelloJava doubleHelloJava = new DoubleHelloJava();
        doubleHelloJava.setHelloJava(helloJava);
        return doubleHelloJava;
    }

    @Bean
    public DoubleHelloJava doubleHelloJava() {
        DoubleHelloJava doubleHelloJava = new DoubleHelloJava();
        doubleHelloJava.setHelloJava(helloJava());
        return doubleHelloJava;
    }
}
