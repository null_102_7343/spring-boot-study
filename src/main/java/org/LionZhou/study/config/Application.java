package org.LionZhou.study.config;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(JavaConfig.class);
        HelloJava helloJava = context.getBean(HelloJava.class);
        System.out.println(helloJava.sayHello("world"));

        DoubleHelloJava doubleHelloJava = context.getBean(DoubleHelloJava.class);
        System.out.println(doubleHelloJava.sayHello("world double"));

        context.close();
    }
}
