package org.LionZhou.study.config;

public class DoubleHelloJava {

    private HelloJava helloJava;

    public String sayHello(String msg) {
        return helloJava.sayHello(msg) + "\n" + helloJava.sayHello(msg);
    }

    public HelloJava getHelloJava() {
        return helloJava;
    }

    public void setHelloJava(HelloJava helloJava) {
        this.helloJava = helloJava;
    }
}
