package org.LionZhou.study.typeSecurityConfig;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(AuthorSettings.class)
public class Config {

//    @Bean
//    public AuthorSettings getAuthorSettings() {
//        return new AuthorSettings();
//    }
}
