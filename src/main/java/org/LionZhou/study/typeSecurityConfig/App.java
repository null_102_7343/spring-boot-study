package org.LionZhou.study.typeSecurityConfig;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

        AuthorSettings authorSettings = context.getBean(AuthorSettings.class);

        System.out.println(authorSettings);

        context.close();
    }
}
