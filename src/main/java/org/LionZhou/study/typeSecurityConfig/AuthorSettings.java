package org.LionZhou.study.typeSecurityConfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@PropertySource("classpath:org/cutelion/study/typeSecurityConfig/typeSecurityConfig.yaml")
@ConfigurationProperties(prefix = "author")
@Component
public class AuthorSettings {

    private String name;
    private Integer age;

    @Override
    public String toString() {
        return "AuthorSettings{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
