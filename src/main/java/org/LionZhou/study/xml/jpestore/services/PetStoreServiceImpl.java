package org.LionZhou.study.xml.jpestore.services;

import org.LionZhou.study.xml.jpestore.dao.jpa.JpaAccountDao;
import org.LionZhou.study.xml.jpestore.dao.jpa.JpaItemDao;

import java.util.List;

public class PetStoreServiceImpl implements PetStoreService {

    private JpaAccountDao accountDao;

    private JpaItemDao itemDao;

    @Override
    public List<String> getUsernameList() {
        return null;
    }

    public JpaAccountDao getAccountDao() {
        return accountDao;
    }

    public void setAccountDao(JpaAccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public JpaItemDao getItemDao() {
        return itemDao;
    }

    public void setItemDao(JpaItemDao itemDao) {
        this.itemDao = itemDao;
    }
}
