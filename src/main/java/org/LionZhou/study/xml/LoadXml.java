package org.LionZhou.study.xml;

import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

public class LoadXml {

    static void xml() {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("daos.xml", "services.xml");

    }

    static void generic() {
        GenericApplicationContext genericApplicationContext = new GenericApplicationContext();
        XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(genericApplicationContext);
        xmlBeanDefinitionReader.loadBeanDefinitions("daos.xml", "services.xml");
        genericApplicationContext.refresh();
    }

    public static void main(String[] args) {
        xml();
    }
}
