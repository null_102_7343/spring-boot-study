package org.LionZhou.study.jpa.repo;

import org.cutelion.study.jpa.domain.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * key words
 * key                  example     JPQL
 * And
 * Or
 * Is,Equals
 * Between
 * LessThan
 * LessThanEqual
 * GreaterThan
 * GreaterThanEqual
 * After
 * Before
 * IsNull
 * IsNotNull,NotNull
 * Like
 * NotLike
 * StartingWith
 * EndingWith
 * Containing
 * OrderBy
 * Not
 * In
 * NotIn
 * True
 * False
 * IgnoreCase
 */
public interface ArticlesRepo extends JpaRepository<Article, Long> {

    // where title = ?1 and id = ?2
    Article findByTitleAndId(String title, Integer id);

    // where title = ?1 or id = ?2
    Article findByTitleOrId(String title, Integer id);

    // where title = ?1
    Article findByTitle(String title);

    Article getByTitleIs(String title);

    Article readByTitleEquals(String title);

    // where title between ?1 and ?2
    List<Article> readByTitleBetween(String title1, String title2);

    // where id < ?1
    List<Article> readByIdLessThan(Integer id);

    // where id <= ?1
    List<Article> readByIdLessThanEqual(Integer id);

    // where id >= ?1
    List<Article> getByIdGreaterThanEqual(Integer id);

    // where created_at > ?1
    List<Article> getByCreatedAtAfter(Date date);

    // where created_at < ?1
    List<Article> getByCreatedAtBefore(Date date);

    // where title is null
    Article queryByTitleIsNull();

    // where title is not null
    Article queryByTitleIsNotNull();

    Article queryByTitleNotNull();

    // where title like ?1 limit 0, 5
    List<Article> getFirst5ByTitleLike(String title);

    // where title not like ?1
    List<Article> queryByTitleNotLike(String title);

    // where title like %{?1}
    List<Article> queryByTitleStartingWith(String title);

    // where title like {?1}%
    List<Article> queryByTitleEndingWith(String title);

    // where title like %{?1}%
    List<Article> queryByTitleContaining(String title);

    // order by title desc
    List<Article> readOrOrderByTitleDesc();

    // where title <> ?1
    List<Article> readByTitleNot(String title);

    // where title in ()
    List<Article> readByTitleIn(Collection<String> titles);

    // where title not in()
    List<Article> readByTitleNotIn(Collection<String> titles);

    // where title is true
    List<Article> readByTitleTrue();

    List<Article> readByTitleIsTrue();

    // where title is false
    List<Article> readByTitleFalse();

    List<Article> readByTitleIsFalse();

    // where title upper(title) = upper(?1)
    List<Article> readByTitleIgnoreCase(String title);

    // limit 0, 30
    List<Article> readFirst30();

    List<Article> getTop30();

    // named query: select * from articles where id > 10 limit 0, 20
    List<Article> getTop20ByIdGreaterThan(Integer id);

    // 不是普通的sql
    @Query("select a from Articles a where id = ?1")
    List<Article> findByIdEqualsWithIndexParams(Integer id);

    @Query("select a from Articles a where id = :aid")
    List<Article> findByIdEqualsWithNamedParams(@Param("aid") Integer id);

    /**
     * update
     * 利用{@link Modifying} 和 {@link Query}组合来更新
     * 
     */
    @Modifying
    @Transactional
    @Query("update Article a set a.title = :newTitle")
    int setTitle(@Param("newTitle") String title);


}
