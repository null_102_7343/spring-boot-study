package org.LionZhou.study.jpa.domain;

import org.hibernate.annotations.NamedQuery;

import javax.persistence.Entity;
import java.util.Date;

@Entity
// 取代对应查询interface中的接口实际的语句
@NamedQuery(name = "Article.getTop20ByIdGreaterThan",
query = "select * from articles where id > 10 limit 0, 20")
public class Article {

    private Integer id;
    private String title;
    private Date createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
