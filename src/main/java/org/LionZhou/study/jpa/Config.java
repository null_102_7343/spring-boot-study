package org.LionZhou.study.jpa;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.persistence.EntityManagerFactory;

@Configuration
// 需要扫描的包
@EnableJpaRepositories("org.cutelion.study.jpa.repo")
public class Config {

    @Bean
    public EntityManagerFactory entityManagerFactory() {

        return null;
    }
}
